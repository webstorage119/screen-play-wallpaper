<?xml version="1.0" encoding="utf-8"?>
<!DOCTYPE TS>
<TS version="2.1" language="de_DE" sourcelanguage="en">
<context>
    <name>Community</name>
    <message>
        <source>News</source>
        <translation>Neuigkeiten</translation>
    </message>
    <message>
        <source>Wiki</source>
        <translation>Wiki</translation>
    </message>
    <message>
        <source>Forum</source>
        <translation>Forum</translation>
    </message>
    <message>
        <source>Contribute</source>
        <translation>Beitragen</translation>
    </message>
    <message>
        <source>Steam Workshop</source>
        <translation>Steam Workshop</translation>
    </message>
    <message>
        <source>Issue Tracker</source>
        <translation type="unfinished">Issue Tracker</translation>
    </message>
    <message>
        <source>Reddit</source>
        <translation type="unfinished">Reddit</translation>
    </message>
</context>
<context>
    <name>CommunityNavItem</name>
    <message>
        <source>Open in browser</source>
        <translation>Öffne in</translation>
    </message>
</context>
<context>
    <name>CreateSidebar</name>
    <message>
        <source>Tools Overview</source>
        <translation type="unfinished">Tools Overview</translation>
    </message>
    <message>
        <source>Video Import h264 (.mp4)</source>
        <translation type="unfinished">Video Import h264 (.mp4)</translation>
    </message>
    <message>
        <source>Video Import VP8 &amp; VP9 (.webm)</source>
        <translation type="unfinished">Video Import VP8 &amp; VP9 (.webm)</translation>
    </message>
    <message>
        <source>Video import (all types)</source>
        <translation type="unfinished">Video import (all types)</translation>
    </message>
    <message>
        <source>GIF Wallpaper</source>
        <translation type="unfinished">GIF Wallpaper</translation>
    </message>
    <message>
        <source>QML Wallpaper</source>
        <translation type="unfinished">QML Wallpaper</translation>
    </message>
    <message>
        <source>HTML5 Wallpaper</source>
        <translation type="unfinished">HTML5 Wallpaper</translation>
    </message>
    <message>
        <source>Website Wallpaper</source>
        <translation type="unfinished">Website Wallpaper</translation>
    </message>
    <message>
        <source>QML Widget</source>
        <translation type="unfinished">QML Widget</translation>
    </message>
    <message>
        <source>HTML Widget</source>
        <translation type="unfinished">HTML Widget</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperInit</name>
    <message>
        <source>Import any video type</source>
        <translation>Importiere Video jeden Typs</translation>
    </message>
    <message>
        <source>Depending on your PC configuration it is better to convert your wallpaper to a specific video codec. If both have bad performance you can also try a QML wallpaper! Supported video formats are: 

*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</source>
        <translation>Je nach deiner PC Konfiguaration ist es besser dein Wallpaper mit einem Spezifischen Video Kodierer zu Konvertieren. Wenn allerdings beides schlecht läuft kannst du ein QML Wallpaper Probieren! Unterstützte Video-Formate sind:
*.mp4  *.mpg *.mp2 *.mpeg *.ogv *.avi *.wmv *.m4v *.3gp *.flv</translation>
    </message>
    <message>
        <source>Set your preffered video codec:</source>
        <translation>Bevorzugte Video-Kodierung Festlegen</translation>
    </message>
    <message>
        <source>Quality slider. Lower value means better quality.</source>
        <translation>Qualitäts-Regler. Niedriger wert heißt niedrige Qualität</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Öffne Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperResult</name>
    <message>
        <source>An error occurred!</source>
        <translation>Es ist ein Fehler aufgetreten!</translation>
    </message>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Kopiere den Text in die Zwischenablage</translation>
    </message>
    <message>
        <source>Back to create and send an error report!</source>
        <translation>Zurück zum Erstellen und einen Fehlerbericht senden!</translation>
    </message>
</context>
<context>
    <name>CreateWallpaperVideoImportConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Erzeuge Vorschaubild...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Erzeuge Vorschau-Miniaturbild...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generiere ein 5-Sekunden-Vorschau-Video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generiere Vorschau-Gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konvertiere Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Video wird umgewandelt... Das kann etwas dauern!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Konvertieren nicht erfolgreich!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analyse des Videos schlug Fehl!</translation>
    </message>
    <message>
        <source>Convert a video to a wallpaper</source>
        <translation>Konvertiere ein Video in ein Hintergrund Live Wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generiere Vorschau-Video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Name (erforderlich!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>YouTube-URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Speicher Wallpaper...</translation>
    </message>
</context>
<context>
    <name>DefaultVideoControls</name>
    <message>
        <source>Volume</source>
        <translation>Lautstärke</translation>
    </message>
    <message>
        <source>Current Video Time</source>
        <translation>Aktuelle Videozeit</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Füll-Modus</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Strecken</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Ausfüllen</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Enthält</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Cover</translation>
    </message>
    <message>
        <source>Scale_Down</source>
        <translation>Runter Skallieren</translation>
    </message>
</context>
<context>
    <name>ExitPopup</name>
    <message>
        <source>Minimize ScreenPlay</source>
        <translation type="unfinished">Minimize ScreenPlay</translation>
    </message>
    <message>
        <source>Always minimize ScreenPlay</source>
        <translation type="unfinished">Always minimize ScreenPlay</translation>
    </message>
    <message>
        <source>You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</source>
        <translation type="unfinished">You have active Wallpaper.
ScreenPlay will only quit if no Wallpaper are running.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the bottom right Tray-Icon.</translation>
    </message>
    <message>
        <source>You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</source>
        <translation type="unfinished">You can &lt;b&gt;quit&lt;/b&gt; ScreenPlay via the top right Tray-Icon.</translation>
    </message>
    <message>
        <source>Quit ScreenPlay now</source>
        <translation type="unfinished">Quit ScreenPlay now</translation>
    </message>
</context>
<context>
    <name>GifWallpaper</name>
    <message>
        <source>Import a Gif Wallpaper</source>
        <translation>Importiere ein GIF Wallpaper</translation>
    </message>
    <message>
        <source>Drop a *.gif file here or use &apos;Select file&apos; below.</source>
        <translation>Ziehe eine .gif Datei hierher und benutze &apos;Datei auswählen&apos; darunter.</translation>
    </message>
    <message>
        <source>Select your gif</source>
        <translation>Wähle dein GIF aus</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
</context>
<context>
    <name>HTMLWallpaper</name>
    <message>
        <source>Create a HTML Wallpaper</source>
        <translation>Erstelle ein HTML Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Lizenz &amp; Schlagwörter</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Vorschau Bild</translation>
    </message>
</context>
<context>
    <name>HTMLWidget</name>
    <message>
        <source>Create a HTML widget</source>
        <translation>Erstelle ein HTML Widget</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Widget Name</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
</context>
<context>
    <name>ImportWebmConvert</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>AnalyseVideo...</source>
        <translation>Analysiere Video...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation>Erzeuge Vorschaubild...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation>Erzeuge Vorschau-Miniaturbild...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation>Generiere ein 5-Sekunden-Vorschau-Video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation>Generiere Vorschau-Gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation>Konvertiere Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation>Video wird umgewandelt... Das kann etwas dauern!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation>Konvertieren nicht erfolgreich!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation>Analyse des Videos schlug Fehl!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation>Importiere ein Video zu ein Wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation>Generiere Vorschau-Video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation>Name (erforderlich!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation>YouTube-URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation>Abbrechen</translation>
    </message>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation>Speicher Wallpaper...</translation>
    </message>
</context>
<context>
    <name>ImportWebmInit</name>
    <message>
        <source>Import a .webm video</source>
        <translation>Importiere ein .webm Video</translation>
    </message>
    <message>
        <source>When importing webm we can skip the long conversion. When you get unsatisfying results with the ScreenPlay importer from &apos;ideo import and convert (all types)&apos; you can also convert via the free and open source HandBrake!</source>
        <translation>Wenn WEBM importiert wird, kann die lange Umwandlungszeit übersprungen werden. Wenn du nicht mit dem Ergebnis von dem ScreenPlay importierer zu frieden bis &apos; Video Importierer und Convertierer&apos; kannst du auch den gratis und Open Source konvertierer HandBreak benutzen!</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid VP8 or VP9 (*.webm)!</source>
        <translation>Ungültiger Dateityp. Es muss ein gültiger VP8 oder Vp9 (*.webm) typ sein!</translation>
    </message>
    <message>
        <source>Drop a *.webm file here or use &apos;Select file&apos; below.</source>
        <translation>Lass hier eine *.webm Datei fallen oder benutze &apos;Datei auswählen&apos; darunter</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation>Öffne Dokumentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation>Datei auswählen</translation>
    </message>
</context>
<context>
    <name>Importh264Convert</name>
    <message>
        <source>AnalyseVideo...</source>
        <translation type="unfinished">AnalyseVideo...</translation>
    </message>
    <message>
        <source>Generating preview image...</source>
        <translation type="unfinished">Generating preview image...</translation>
    </message>
    <message>
        <source>Generating preview thumbnail image...</source>
        <translation type="unfinished">Generating preview thumbnail image...</translation>
    </message>
    <message>
        <source>Generating 5 second preview video...</source>
        <translation type="unfinished">Generating 5 second preview video...</translation>
    </message>
    <message>
        <source>Generating preview gif...</source>
        <translation type="unfinished">Generating preview gif...</translation>
    </message>
    <message>
        <source>Converting Audio...</source>
        <translation type="unfinished">Converting Audio...</translation>
    </message>
    <message>
        <source>Converting Video... This can take some time!</source>
        <translation type="unfinished">Converting Video... This can take some time!</translation>
    </message>
    <message>
        <source>Converting Video ERROR!</source>
        <translation type="unfinished">Converting Video ERROR!</translation>
    </message>
    <message>
        <source>Analyse Video ERROR!</source>
        <translation type="unfinished">Analyse Video ERROR!</translation>
    </message>
    <message>
        <source>Import a video to a wallpaper</source>
        <translation type="unfinished">Import a video to a wallpaper</translation>
    </message>
    <message>
        <source>Generating preview video...</source>
        <translation type="unfinished">Generating preview video...</translation>
    </message>
    <message>
        <source>Name (required!)</source>
        <translation type="unfinished">Name (required!)</translation>
    </message>
    <message>
        <source>Description</source>
        <translation type="unfinished">Description</translation>
    </message>
    <message>
        <source>Youtube URL</source>
        <translation type="unfinished">Youtube URL</translation>
    </message>
    <message>
        <source>Abort</source>
        <translation type="unfinished">Abort</translation>
    </message>
    <message>
        <source>Save</source>
        <translation type="unfinished">Save</translation>
    </message>
    <message>
        <source>Save Wallpaper...</source>
        <translation type="unfinished">Save Wallpaper...</translation>
    </message>
</context>
<context>
    <name>Importh264Init</name>
    <message>
        <source>Import a .mp4 video</source>
        <translation type="unfinished">Import a .mp4 video</translation>
    </message>
    <message>
        <source>ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</source>
        <translation type="unfinished">ScreenPlay V0.15 and up can play *.mp4 (also more known as h264). This can improove performance on older systems.</translation>
    </message>
    <message>
        <source>Invalid file type. Must be valid h264 (*.mp4)!</source>
        <translation type="unfinished">Invalid file type. Must be valid h264 (*.mp4)!</translation>
    </message>
    <message>
        <source>Drop a *.mp4 file here or use &apos;Select file&apos; below.</source>
        <translation type="unfinished">Drop a *.mp4 file here or use &apos;Select file&apos; below.</translation>
    </message>
    <message>
        <source>Open Documentation</source>
        <translation type="unfinished">Open Documentation</translation>
    </message>
    <message>
        <source>Select file</source>
        <translation type="unfinished">Select file</translation>
    </message>
</context>
<context>
    <name>Installed</name>
    <message>
        <source></source>
        <translation></translation>
    </message>
    <message>
        <source>Refreshing!</source>
        <translation>Aktualisiere!</translation>
    </message>
    <message>
        <source>Pull to refresh!</source>
        <translation>Drücken zum aktualisieren!</translation>
    </message>
    <message>
        <source>Get more Wallpaper &amp; Widgets via the Steam workshop!</source>
        <translation>Holen dir mehr Wallpaper und Widgets über den Steam-Workshop!</translation>
    </message>
    <message>
        <source>Open containing folder</source>
        <translation>Enthaltenden Ordner öffnen</translation>
    </message>
    <message>
        <source>Remove Item</source>
        <translation>Item entfernen</translation>
    </message>
    <message>
        <source>Remove via Workshop</source>
        <translation>Über den Workshop entfernen</translation>
    </message>
    <message>
        <source>Open Workshop Page</source>
        <translation>Workshop öffnen</translation>
    </message>
    <message>
        <source>Are you sure you want to delete this item?</source>
        <translation>Bist du dir sicher dass du dieses Item löschen möchtest?</translation>
    </message>
    <message>
        <source>Export</source>
        <translation type="unfinished">Export</translation>
    </message>
    <message>
        <source>We only support adding one item at once.</source>
        <translation type="unfinished">We only support adding one item at once.</translation>
    </message>
    <message>
        <source>File type not supported. We only support &apos;.screenplay&apos; files.</source>
        <translation type="unfinished">File type not supported. We only support &apos;.screenplay&apos; files.</translation>
    </message>
    <message>
        <source>Import Content...</source>
        <translation type="unfinished">Import Content...</translation>
    </message>
    <message>
        <source>Export Content...</source>
        <translation type="unfinished">Export Content...</translation>
    </message>
</context>
<context>
    <name>InstalledNavigation</name>
    <message>
        <source>All</source>
        <translation type="unfinished">All</translation>
    </message>
    <message>
        <source>Scenes</source>
        <translation type="unfinished">Scenes</translation>
    </message>
    <message>
        <source>Videos</source>
        <translation type="unfinished">Videos</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation type="unfinished">Widgets</translation>
    </message>
</context>
<context>
    <name>InstalledWelcomeScreen</name>
    <message>
        <source>Get free Widgets and Wallpaper via the Steam Workshop</source>
        <translation>Hole dir kostenlose Widgets und Wallpaper via Steam</translation>
    </message>
    <message>
        <source>Browse the Steam Workshop</source>
        <translation>Stöbere durch den Steam Workshop</translation>
    </message>
    <message>
        <source>Get content via our forum</source>
        <translation type="unfinished">Get content via our forum</translation>
    </message>
    <message>
        <source>Open the ScreenPlay forum</source>
        <translation type="unfinished">Open the ScreenPlay forum</translation>
    </message>
</context>
<context>
    <name>Monitors</name>
    <message>
        <source>Wallpaper Configuration</source>
        <translation>Wallpaper Konfiguration</translation>
    </message>
    <message>
        <source>Remove selected</source>
        <translation>Die Auswahl entfernen</translation>
    </message>
    <message>
        <source>Wallpapers</source>
        <translation>Hintergründe</translation>
    </message>
    <message>
        <source>Widgets</source>
        <translation>Widgets</translation>
    </message>
    <message>
        <source>Remove all </source>
        <translation type="unfinished">Remove all </translation>
    </message>
</context>
<context>
    <name>MonitorsProjectSettingItem</name>
    <message>
        <source>Set color</source>
        <translation>Farbe Festlegen</translation>
    </message>
    <message>
        <source>Please choose a color</source>
        <translation>Bitte wähle eine Farbe aus</translation>
    </message>
</context>
<context>
    <name>Navigation</name>
    <message>
        <source>Create</source>
        <translation>Erstellen</translation>
    </message>
    <message>
        <source>Workshop</source>
        <translation>Workshop</translation>
    </message>
    <message>
        <source>Installed</source>
        <translation>Installiert</translation>
    </message>
    <message>
        <source>Community</source>
        <translation>Community</translation>
    </message>
    <message>
        <source>Settings</source>
        <translation>Einstellungen</translation>
    </message>
    <message>
        <source>Mute/Unmute all Wallpaper</source>
        <translation type="unfinished">Mute/Unmute all Wallpaper</translation>
    </message>
    <message>
        <source>Pause/Play all Wallpaper</source>
        <translation type="unfinished">Pause/Play all Wallpaper</translation>
    </message>
    <message>
        <source>Configure Wallpaper</source>
        <translation type="unfinished">Configure Wallpaper</translation>
    </message>
    <message>
        <source>Support me on Patreon!</source>
        <translation type="unfinished">Support me on Patreon!</translation>
    </message>
    <message>
        <source>Close All Content</source>
        <translation type="unfinished">Close All Content</translation>
    </message>
</context>
<context>
    <name>QMLWallpaper</name>
    <message>
        <source>Create a QML Wallpaper</source>
        <translation>Erstelle ein QML Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>License &amp; Tags</source>
        <translation>Lizenz &amp; Schlagwörter</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Vorschaubild</translation>
    </message>
</context>
<context>
    <name>QMLWallpaperMain</name>
    <message>
        <source>My ScreenPlay Wallpaper 🚀</source>
        <translation type="unfinished"></translation>
    </message>
</context>
<context>
    <name>QMLWidget</name>
    <message>
        <source>Create a QML widget</source>
        <translation>Erstelle ein QML Widget</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Widget name</source>
        <translation>Widget Name</translation>
    </message>
    <message>
        <source>Created by</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
</context>
<context>
    <name>QMLWidgetMain</name>
    <message>
        <source>My Widget 🚀</source>
        <translation type="unfinished">My Widget 🚀</translation>
    </message>
</context>
<context>
    <name>SaveNotification</name>
    <message>
        <source>Profile saved successfully!</source>
        <translation>Profil erfolgreich gespeichert!</translation>
    </message>
</context>
<context>
    <name>ScreenPlayItem</name>
    <message>
        <source>NEW</source>
        <translation>NEU</translation>
    </message>
</context>
<context>
    <name>Settings</name>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Autostart</source>
        <translation>Autostart</translation>
    </message>
    <message>
        <source>ScreenPlay will start with Windows and will setup your Desktop every time for you.</source>
        <translation>ScreenPlay startet mit Windows und richtet deinen Desktop jedes Mal für dich ein.</translation>
    </message>
    <message>
        <source>High priority Autostart</source>
        <translation>Hohe Priorität für Autostart</translation>
    </message>
    <message>
        <source>This options grants ScreenPlay a higher autostart priority than other apps.</source>
        <translation>Diese Option gewährt ScreenPlay eine höhere Autostartpriorität als anderen Anwendungen.</translation>
    </message>
    <message>
        <source>Send anonymous crash reports and statistics</source>
        <translation>Sende anonyme Absturzberichte und Statistiken</translation>
    </message>
    <message>
        <source>Help us make ScreenPlay faster and more stable. All collected data is purely anonymous and only used for development purposes! We use &lt;a href=&quot;https://sentry.io&quot;&gt;sentry.io&lt;/a&gt; to collect and analyze this data. A &lt;b&gt;big thanks to them&lt;/b&gt; for providing us with free premium support for open source projects!</source>
        <translation>Helfen Sie uns, ScreenPlay schneller und stabiler zu machen. Alle gesammelten Daten sind rein anonym und werden nur für Entwicklungszwecke verwendet! Wir benutzen &lt;a href=&quot;https://sentry.io&quot;&gt;Sentry. o&lt;/a&gt; um diese Daten zu sammeln und zu analysieren. Ein &lt;b&gt;großes Dankeschön an sie&lt;/b&gt; für die kostenlose Premium-Unterstützung für Open Source Projekte!</translation>
    </message>
    <message>
        <source>Set save location</source>
        <translation>Speicherort auswählen</translation>
    </message>
    <message>
        <source>Set location</source>
        <translation>Standort auswählen</translation>
    </message>
    <message>
        <source>Your storage path is empty!</source>
        <translation>Dein Speicherpfad ist leer!</translation>
    </message>
    <message>
        <source>Important: Changing this directory has no effect on the workshop download path. ScreenPlay only supports having one content folder!</source>
        <translation>Wichtig: Eine Änderung dieses Verzeichnisses hat keine Auswirkungen auf den Download-Pfad des Workshops. ScreenPlay unterstützt nur ein Verzeichnis!</translation>
    </message>
    <message>
        <source>Language</source>
        <translation>Sprache</translation>
    </message>
    <message>
        <source>Set the ScreenPlay UI Language</source>
        <translation>Wähle die Sprache des Programms aus</translation>
    </message>
    <message>
        <source>Theme</source>
        <translation>Thema</translation>
    </message>
    <message>
        <source>Switch dark/light theme</source>
        <translation>Wechsle Dunkles/Helles Design</translation>
    </message>
    <message>
        <source>System Default</source>
        <translation>System Standard</translation>
    </message>
    <message>
        <source>Dark</source>
        <translation>Dunkel</translation>
    </message>
    <message>
        <source>Light</source>
        <translation>Hell</translation>
    </message>
    <message>
        <source>Performance</source>
        <translation>Leistung</translation>
    </message>
    <message>
        <source>Pause wallpaper video rendering while another app is in the foreground</source>
        <translation>Pausiere Wallpaper Video Rendering wenn eine andere App im Vordergrund ist</translation>
    </message>
    <message>
        <source>We disable the video rendering (not the audio!) for the best performance. If you have problem you can disable this behaviour here. Wallpaper restart required!</source>
        <translation>Wir deaktivieren das Video Rendering (Aber nicht die Sounds) für die beste Leistung. Wenn du damit probleme haben solltest kannst dieses Verhalten hier ausschalten. Ein Neustart wird aber von Nöten sein!</translation>
    </message>
    <message>
        <source>Default Fill Mode</source>
        <translation>Standard-Füllmodus</translation>
    </message>
    <message>
        <source>Set this property to define how the video is scaled to fit the target area.</source>
        <translation>Lege diese Eigenschaft fest, um zu definieren, wie das Video skaliert wird, damit es in den Zielbereich passt.</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Strecken</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Ausfüllen</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Enthält</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Runter Skallieren</translation>
    </message>
    <message>
        <source>About</source>
        <translation>Über</translation>
    </message>
    <message>
        <source>Thank you for using ScreenPlay</source>
        <translation>Danke, dass du ScreenPlay verwendest</translation>
    </message>
    <message>
        <source>Hi, I&apos;m Elias Steurer also known as Kelteseth and I&apos;m the developer of ScreenPlay. Thank you for using my software. You can follow me to receive updates about ScreenPlay here:</source>
        <translation>Moin, ich bin Elias Steurer, auch bekannt als Kelteseth und ich bin der Entwickler von ScreenPlay. Danke, dass du meine Software nutzt. Du kannst mir hier folgen, um Updates über ScreenPlay zu erhalten:</translation>
    </message>
    <message>
        <source>Version</source>
        <translation>Version</translation>
    </message>
    <message>
        <source>Open Changelog</source>
        <translation>Changelog öffnen</translation>
    </message>
    <message>
        <source>Third Party Software</source>
        <translation>Software von Drittanbietern</translation>
    </message>
    <message>
        <source>ScreenPlay would not be possible without the work of others. A big thank you to: </source>
        <translation>ScreenPlay wäre ohne die Arbeit anderer nicht möglich. Ein großes Dankeschön dafür geht an: </translation>
    </message>
    <message>
        <source>Licenses</source>
        <translation>Lizenzen</translation>
    </message>
    <message>
        <source>Logs</source>
        <translation>Protokolle</translation>
    </message>
    <message>
        <source>If your ScreenPlay missbehaves this is a good way to look for answers. This shows all logs and warning during runtime.</source>
        <translation>Wenn den ScreenPlay sich falsch verhält, ist hier eine gute Möglichkeit, nach Antworten zu suchen. Hier werden alle Protokolle und Warnungen während der Laufzeit angezeigt.</translation>
    </message>
    <message>
        <source>Show Logs</source>
        <translation>Zeige Logs</translation>
    </message>
    <message>
        <source>Data Protection</source>
        <translation>Datenschutz</translation>
    </message>
    <message>
        <source>We use you data very carefully to improve ScreenPlay. We do not sell or share this (anonymous) information with others!</source>
        <translation>Wir verwenden deine Daten sehr sorgfältig, um ScreenPlay zu verbessern. Wir verkaufen oder teilen diese (anonymen) Informationen nicht mit anderen!</translation>
    </message>
    <message>
        <source>Privacy</source>
        <translation>Datenschutz</translation>
    </message>
</context>
<context>
    <name>SettingsExpander</name>
    <message>
        <source>Copy text to clipboard</source>
        <translation>Kopiere den Text in die Zwischenablage</translation>
    </message>
</context>
<context>
    <name>Sidebar</name>
    <message>
        <source>Set Wallpaper</source>
        <translation>Wallpaper Festlegen</translation>
    </message>
    <message>
        <source>Set Widget</source>
        <translation>Widget wählen</translation>
    </message>
    <message>
        <source>Headline</source>
        <translation>Überschrift</translation>
    </message>
    <message>
        <source>Select a Monitor to display the content</source>
        <translation>Wähle einen Monitor zur Anzeige des Inhalts</translation>
    </message>
    <message>
        <source>Set Volume</source>
        <translation>Audiolautstärke einstellen</translation>
    </message>
    <message>
        <source>Fill Mode</source>
        <translation>Füll-Modus</translation>
    </message>
    <message>
        <source>Stretch</source>
        <translation>Strecken</translation>
    </message>
    <message>
        <source>Fill</source>
        <translation>Ausfüllen</translation>
    </message>
    <message>
        <source>Contain</source>
        <translation>Enthält</translation>
    </message>
    <message>
        <source>Cover</source>
        <translation>Cover</translation>
    </message>
    <message>
        <source>Scale-Down</source>
        <translation>Runter-Skallieren</translation>
    </message>
</context>
<context>
    <name>StartInfo</name>
    <message>
        <source>Free tools to help you to create wallpaper</source>
        <translation type="unfinished">Free tools to help you to create wallpaper</translation>
    </message>
    <message>
        <source>Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</source>
        <translation type="unfinished">Below you can find tools to create wallaper, beyond the tools that ScreenPlay provides for you!</translation>
    </message>
</context>
<context>
    <name>WebsiteWallpaper</name>
    <message>
        <source>Create a Website Wallpaper</source>
        <translation>Erstelle ein Website Wallpaper</translation>
    </message>
    <message>
        <source>General</source>
        <translation>Allgemein</translation>
    </message>
    <message>
        <source>Wallpaper name</source>
        <translation>Wallpaper Name</translation>
    </message>
    <message>
        <source>Created By</source>
        <translation>Erstellt von</translation>
    </message>
    <message>
        <source>Description</source>
        <translation>Beschreibung</translation>
    </message>
    <message>
        <source>Tags</source>
        <translation>Schlagwörter</translation>
    </message>
    <message>
        <source>Preview Image</source>
        <translation>Vorschaubild</translation>
    </message>
</context>
<context>
    <name>WizardPage</name>
    <message>
        <source>Save</source>
        <translation>Speichern</translation>
    </message>
    <message>
        <source>Saving...</source>
        <translation>Speichern...</translation>
    </message>
</context>
<context>
    <name>XMLNewsfeed</name>
    <message>
        <source>News &amp; Patchnotes</source>
        <translation>Neuigkeiten &amp; Patchnotes</translation>
    </message>
</context>
</TS>
